package com.rotripio.poc.controllers;

import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;

import com.rotripio.poc.domain.Fund;
import com.rotripio.poc.services.FundService;

@Controller
public class FundsController {

	@Autowired
	private View jsonView_i;

	@Autowired
	private FundService fundService_i;

	private static final String DATA_FIELD = "data";
	private static final String ERROR_FIELD = "error";

	private static final Logger logger = Logger
			.getLogger(FundsController.class);

	@RequestMapping(value = "/rest/funds/{fundId}", method = RequestMethod.GET)
	public ModelAndView getFund(@PathVariable("fundId") String fundId_p) {

		Fund fund = null;

		if (isEmpty(fundId_p) || fundId_p.length() < 5) {
			String sMessage = "Error invoking getFund - Invalid fund Id paramter";
			return createErrorResponse(sMessage);
		}

		try {
			fund = fundService_i.getFundById(fundId_p);
		} catch (Exception ex) {
			String sMessage = "Error invoking getFund [%1$s]";
			return createErrorResponse(String.format(sMessage, ex.toString()));
		}

		logger.debug("Returning Fund: " + fund);
		return new ModelAndView(jsonView_i, DATA_FIELD, fund);
	}

	@RequestMapping(value = "/rest/funds/", method = RequestMethod.GET)
	public ModelAndView getFunds() {
		List<Fund> funds = null;
		try {
			funds = fundService_i.getAllFunds();
		} catch (Exception ex) {
			String sMessage = "Error getting all funds. [%1$s]";
			return createErrorResponse(String.format(sMessage, ex.toString()));
		}

		logger.debug("Returning Funds: " + funds);
		return new ModelAndView(jsonView_i, DATA_FIELD, funds);
	}

	@RequestMapping(value = "/rest/funds/", method = RequestMethod.POST)
	public ModelAndView createFund(@RequestBody Fund fund_p,
			HttpServletResponse response, WebRequest request) {
		
		logger.debug("Creating Fund: "+ fund_p.toString());
		
		Fund fund = null;
		try {
			fund = fundService_i.createFund(fund_p);
		} catch (Exception ex) {
			String sMessage = "Error creating new fund. [%1$s]";
			return createErrorResponse(String.format(sMessage, ex.toString()));
		}
		
		response.setStatus(HttpStatus.CREATED.value());
		response.setHeader("Location", request.getContextPath() + "/rest/funds/");

		return new ModelAndView(jsonView_i, DATA_FIELD, fund);
	}
	
	@RequestMapping(value = "/rest/funds/{fundId}", method = RequestMethod.PUT)
	public ModelAndView updateFund(@RequestBody Fund fund_p,
			@PathVariable("fundId") String fundId,  HttpServletResponse response) {
		logger.debug("Updating fund: " + fund_p.toString());
		
		if (isEmpty(fundId) || fundId.length() < 5) {
			String sMessage = "Error updating fund - Invalid fund Id paramter";
			return createErrorResponse(sMessage);
		}
		
		Fund fund = null;
		
		try {
			fund = fundService_i.updateFund(fund_p);
		} catch (Exception e) {
			String sMessage = "Error updating fund. [%1$s]";
			return createErrorResponse(String.format(sMessage, e.toString()));
		}
		
		response.setStatus(HttpStatus.OK.value());
		return new ModelAndView(jsonView_i, DATA_FIELD, fund);
	}

	public static boolean isEmpty(String s_p) {
		return (null == s_p) || s_p.trim().length() == 0;
	}

	/**
	 * Create an error REST response.
	 * 
	 * @param sMessage
	 *            the s message
	 * @return the model and view
	 */
	private ModelAndView createErrorResponse(String sMessage) {
		return new ModelAndView(jsonView_i, ERROR_FIELD, sMessage);
	}
}
